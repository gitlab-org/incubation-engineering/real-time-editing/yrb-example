# frozen_string_literal: true

class RenameUpdateToDiff < ActiveRecord::Migration[7.0]
  def change
    rename_column :diffs, :update, :diff
  end
end
