# frozen_string_literal: true

class CreateChanges < ActiveRecord::Migration[7.0]
  def change
    create_table :changes do |t|
      t.text :diff

      t.timestamps
    end
  end
end
