# frozen_string_literal: true

class CreateDocumentChanges < ActiveRecord::Migration[7.0]
  def change
    create_table :document_changes do |t|
      t.text :delta, null: false
      t.references :document, null: false, foreign_key: { on_delete: :cascade }
      t.references :change, null: false, foreign_key: { on_delete: :restrict }
      t.timestamps
    end

    add_index :document_changes, %w[document_id change_id], unique: true
  end
end
