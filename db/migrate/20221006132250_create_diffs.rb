# frozen_string_literal: true

class CreateDiffs < ActiveRecord::Migration[7.0]
  def change
    create_table :diffs do |t|
      t.string :update
      t.references :document, null: false, foreign_key: true

      t.timestamps
    end
  end
end
