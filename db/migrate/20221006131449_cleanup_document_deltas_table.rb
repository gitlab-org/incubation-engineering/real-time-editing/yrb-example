# frozen_string_literal: true

class CleanupDocumentDeltasTable < ActiveRecord::Migration[7.0]
  def change
    remove_column :document_deltas, :delta
    rename_column :document_deltas, :change_id, :delta_id
  end
end
