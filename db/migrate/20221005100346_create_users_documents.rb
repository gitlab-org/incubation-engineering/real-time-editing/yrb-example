# frozen_string_literal: true

class CreateUsersDocuments < ActiveRecord::Migration[7.0]
  def change
    create_table :users_documents do |t|
      t.references :user, null: false, foreign_key: { on_delete: :cascade }
      t.references :document, null: false, foreign_key: { on_delete: :cascade }
      t.integer :role, null: false, default: 0

      t.timestamps
    end

    add_index :users_documents, %w[user_id document_id], unique: true
  end
end
