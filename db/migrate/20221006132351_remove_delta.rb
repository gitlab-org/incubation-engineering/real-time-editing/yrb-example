# frozen_string_literal: true

class RemoveDelta < ActiveRecord::Migration[7.0]
  def change
    drop_table :document_deltas
    drop_table :deltas
  end
end
