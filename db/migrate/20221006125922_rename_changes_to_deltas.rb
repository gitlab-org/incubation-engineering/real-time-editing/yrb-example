# frozen_string_literal: true

class RenameChangesToDeltas < ActiveRecord::Migration[7.0]
  def change
    rename_table :changes, :deltas
    rename_table :document_changes, :document_deltas
  end
end
