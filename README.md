# Docs

> A demo application for `y-rb`. Built with Rails, Hotwire, Turbo and Stimulus.

![GitLab Docs Sandbox](./docs/assets/screenshot-1.png)
