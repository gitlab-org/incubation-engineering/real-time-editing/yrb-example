# frozen_string_literal: true

source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "3.1.2"

gem "bcrypt", "~> 3.1.7"
gem "bootsnap", require: false
gem "cancancan", "~> 3.4.0"
gem "cssbundling-rails", "~> 1.1"
gem "devise", "~> 4.8.1"
gem "image_processing", "~> 1.2"
gem "jbuilder"
gem "jsbundling-rails"
gem "omniauth-gitlab", "~> 4.1.0"
gem "omniauth-rails_csrf_protection", "~> 1.0.1"
gem "puma", "~> 5.0"
gem "rack-protection", "~> 2.2.2"
gem "rails", "~> 7.0.4"
gem "redis", "~> 4.0"
gem "sassc-rails"
gem "sidekiq", "~> 6.5.7"
gem "sprockets-rails"
gem "sqlite3", "~> 1.4"
gem "stimulus-rails"
gem "tailwindcss-rails", "~> 2.0"
gem "turbo-rails"
gem "tzinfo-data", platforms: %i[mingw mswin x64_mingw jruby]
gem "y-rb", "~> 0.1.7"

group :development, :test do
  # See https://guides.rubyonrails.org/debugging_rails_applications.html#debugging-with-the-debug-gem
  gem "debug", platforms: %i[mri mingw x64_mingw]
  gem "factory_bot_rails", "~> 6.2.0", group: :test
  gem "faker", "~> 2.23.0", require: false, group: :test
  gem "rspec-json_expectations", "~> 2.2.0", group: :test
  gem "rspec-rails", "~> 5.1.2", group: :test
  gem "shoulda-matchers", "~> 5.2.0", group: :test
  gem "simplecov", require: false, group: :test
  gem "simplecov-cobertura", require: false, group: :test
end

group :development do
  gem "annotate"
  gem "foreman"
  gem "letter_opener"
  # gem "rack-mini-profiler"
  gem "rubocop"
  gem "rubocop-faker"
  gem "rubocop-performance"
  gem "rubocop-rails"
  gem "rubocop-rspec"
  gem "solargraph"
  gem "web-console"
end

group :test do
  gem "capybara"
  gem "selenium-webdriver"
  gem "webdrivers"
end
