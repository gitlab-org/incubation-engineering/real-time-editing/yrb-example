# frozen_string_literal: true

class UsersDocument < ApplicationRecord
  belongs_to :user
  belongs_to :document
end
