# frozen_string_literal: true

class Document < ApplicationRecord
  has_many :users_documents
  has_many :users, through: :users_documents

  has_many :diffs

  accepts_nested_attributes_for :users_documents

  delegate :diff, to: :ydoc
  delegate :full_diff, to: :ydoc
  delegate :state, to: :ydoc
  delegate :sync, to: :ydoc

  Heading = Struct.new(:level, :title)

  def headers
    find_headers(element).map do |header|
      level = header.attributes["level"].to_i
      title = extract_text(header)

      Heading.new(level, title)
    end
  end

  private

  def element
    @element ||= ydoc.get_xml_element("default")
  end

  def ydoc
    @ydoc ||= begin
      doc = Y::Doc.new
      diffs.pluck(:diff).each { |diff| doc.sync(decode(diff)) }
      doc
    end
  end

  def decode(diff)
    diff.split(",").map(&:to_i)
  end

  def find_headers(element)
    node = element.first_child
    list = []

    loop do
      next unless node.instance_of?(Y::XMLElement)

      list << node if node.tag == "heading"

      node = node.next_sibling
      break if node.blank?
    end

    list
  end

  def extract_text(element)
    # node = element.first_child
    # text = []
    #
    # loop do
    #   next unless node.instance_of?(Y::XMLText)
    #
    #   text << node.to_s
    #
    #   node = node.next_sibling
    #   break if node.blank?
    # end
    #
    # text.join
    element.first_child.to_s
  end
end
