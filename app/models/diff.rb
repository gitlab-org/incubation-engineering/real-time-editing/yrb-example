# frozen_string_literal: true

class Diff < ApplicationRecord
  belongs_to :document

  after_commit :broadcast_document_change

  private

  def broadcast_document_change
    broadcast_replace_to(
      "documents/#{document.id}/outline",
      target: "document_#{document.id}",
      partial: "documents/outline/show",
      locals: { document: }
    )
  end
end
