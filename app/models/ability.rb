# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    can :read, Document, ["users_documents.role > ?", 0]
  end
end
