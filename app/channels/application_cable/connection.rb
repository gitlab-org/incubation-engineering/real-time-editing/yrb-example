# frozen_string_literal: true

module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    private

    def current_user
      if (user = env["warden"].user)
        user
      else
        reject_unauthorized_connection
      end
    end
  end
end
