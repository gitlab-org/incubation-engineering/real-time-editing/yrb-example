# frozen_string_literal: true

class DocumentChannel < ApplicationCable::Channel
  def subscribed
    reject unless valid_subscription?

    return if subscription_rejected?

    stream_for document

    initial_update = document.full_diff
    DocumentChannel.broadcast_to(document, initial_update)
  end

  def unsubscribed
  end

  def receive(data)
    update = data["update"]

    old_state = document.state
    document.sync(update)

    diff = document.diff(old_state)
    document.diffs.create!(diff: diff.join(","))

    DocumentChannel.broadcast_to(document, diff)
  end

  private

  def clock!
    @clock ||= 0
    @clock += 1
    @clock
  end

  def clock
    @clock ||= 1
  end

  def valid_subscription?
    current_user.present? && id.present?
  end

  def document
    @document ||= Document.find_or_create_by!(id:)
  end

  def id
    params[:id]
  end
end
