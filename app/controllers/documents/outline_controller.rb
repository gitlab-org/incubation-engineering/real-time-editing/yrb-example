# frozen_string_literal: true

module Documents
  class OutlineController < ApplicationController
    before_action :set_document, only: %i[show]

    def show
    end

    private

    def set_document
      @document = Document.find(params[:document_id])
    end
  end
end
