# frozen_string_literal: true

class HomeController < ApplicationController
  before_action :authenticate_user!

  def index
    @documents = current_user.documents
                             .accessible_by(current_ability)
                             .order(:updated_at, :desc)
                             .limit(9)
  end
end
