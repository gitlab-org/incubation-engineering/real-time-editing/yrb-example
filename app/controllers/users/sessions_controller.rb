# frozen_string_literal: true

module Users
  class SessionsController < Devise::SessionsController
    layout "fullpage"

    # before_action :configure_sign_in_params, only: [:create]

    IMAGES = [
      "https://images.unsplash.com/photo-1575980831061-a5605b5ea109?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1908&q=80"
    ].freeze
    private_constant :IMAGES

    # GET /resource/sign_in
    def new
      super

      @current_image = IMAGES.sample
    end

    # POST /resource/sign_in
    # def create
    #   super
    # end

    # DELETE /resource/sign_out
    # def destroy
    #   super
    # end

    # protected

    # If you have extra params to permit, append them to the sanitizer.
    # def configure_sign_in_params
    #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
    # end
  end
end
