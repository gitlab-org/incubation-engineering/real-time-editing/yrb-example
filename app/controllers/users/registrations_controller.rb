# frozen_string_literal: true

module Users
  class RegistrationsController < Devise::RegistrationsController
    layout :resolve_layout

    # before_action :configure_sign_up_params, only: [:create]
    before_action :configure_account_update_params, only: [:update]

    # GET /resource/sign_up
    # def new
    #   super
    # end

    # POST /resource
    # def create
    #   super
    # end

    # GET /resource/edit
    # def edit
    #   super
    # end

    # PUT /resource

    # DELETE /resource
    # def destroy
    #   super
    # end

    # GET /resource/cancel
    # Forces the session data which is usually expired after sign
    # in to be expired now. This is useful if the user wants to
    # cancel oauth signing in/up in the middle of the process,
    # removing all OAuth session data.
    # def cancel
    #   super
    # end

    protected

    # If you have extra params to permit, append them to the sanitizer.
    # def configure_sign_up_params
    #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
    # end

    # The path used after sign up.
    # def after_sign_up_path_for(resource)
    #   super(resource)
    # end

    # The path used after sign up for inactive accounts.
    # def after_inactive_sign_up_path_for(resource)
    #   super(resource)
    # end

    def after_update_path_for(_resource)
      sign_in_after_change_password? ? edit_user_registration_path : new_session_path(resource_name)
    end

    def update_resource(resource, params)
      if %w[gitlab].include?(current_user.provider)
        params.delete("current_password")
        resource.update_without_password(params)
      else
        resource.update_with_password(params)
      end
    end

    private

    def configure_account_update_params
      devise_parameter_sanitizer.permit(:account_update, keys: [:avatar])
    end

    def resolve_layout
      case action_name
      when "new"
        "fullpage"
      else
        "application"
      end
    end
  end
end
