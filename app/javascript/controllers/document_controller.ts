import {Controller} from "@hotwired/stimulus";
import {Editor} from "@tiptap/core";
// import CollaborationCursor from '@tiptap/extension-collaboration-cursor'
import CodeBlockLowLight from "@tiptap/extension-code-block-lowlight";
import {Collaboration} from "@tiptap/extension-collaboration";
import {Link} from "@tiptap/extension-link";
import {Mention} from "@tiptap/extension-mention";
import {StarterKit} from "@tiptap/starter-kit";
import css from "highlight.js/lib/languages/css";
import js from "highlight.js/lib/languages/javascript";
import ruby from "highlight.js/lib/languages/ruby";
import rust from "highlight.js/lib/languages/rust";
import ts from "highlight.js/lib/languages/typescript";
import html from "highlight.js/lib/languages/xml";
import {lowlight} from "lowlight/lib/core";
import * as Y from "yjs";

import {consumer} from "../channels/consumer";

lowlight.registerLanguage("html", html);
lowlight.registerLanguage("css", css);
lowlight.registerLanguage("js", js);
lowlight.registerLanguage("ruby", ruby);
lowlight.registerLanguage("rust", rust);
lowlight.registerLanguage("ts", ts);

export default class extends Controller {
  static targets = [
    "canvas",
    "rowMarker"
  ];
  static values = {
    id: Number
  };

  declare readonly idValue: number;
  declare readonly canvasTarget: Element;
  declare readonly rowMarkerTarget: HTMLElement;

  private editor: Editor | undefined;

  connect() {
    this.scrollTo.bind(this);
    const rowMarker = this.rowMarkerTarget;

    const doc = new Y.Doc();
    doc.on("update", (update: ArrayLike<number>) => {
      documentChannel.send({update: Array.from(update)});
    });

    const documentChannel = consumer.subscriptions
      .create({channel: "DocumentChannel", id: this.idValue}, {
        received(data: Iterable<number>) {
          const update = Uint8Array.from(data);
          Y.applyUpdate(doc, update);
        }
      });

    this.editor = new Editor({
      onSelectionUpdate({ editor }) {
        const {top} = editor.view.coordsAtPos(editor.state.selection.anchor);
        rowMarker.style.top = `${top - 96 - 16}px`;
      },
      autofocus: true,
      element: this.canvasTarget,
      extensions: [
        StarterKit.configure({
          codeBlock: false,
          // The Collaboration extension comes with its own history handling
          history: false,
        }),
        CodeBlockLowLight.configure({lowlight}),
        Collaboration.configure({document: doc}),
        // , CollaborationCursor.configure({
        //     user: {
        //         provider, name: userid, color: '#f783ac',
        //     },
        // })
        Link.configure({
          protocols: ["http", "https", "mailto"],
        }),
        Mention,
      ]
    });
  }

  disconnect() {
    this.editor?.destroy();
  }

  scrollTo(event: MouseEvent) {
    event.preventDefault();
  }
}
