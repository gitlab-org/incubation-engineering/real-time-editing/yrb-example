import {application} from "./application";
import DocumentController from "./document_controller";

application.register("document", DocumentController);
