# frozen_string_literal: true

module ProfileImageUrlHelper
  def profile_image_url(user, size = 128)
    if user.avatar.attached?
      user.avatar.variant(resize_to_limit: [size, size])
    else
      hash = Digest::MD5.hexdigest(user.email.downcase)
      "https://www.gravatar.com/avatar/#{hash}.png?s=#{size}"
    end
  end
end
