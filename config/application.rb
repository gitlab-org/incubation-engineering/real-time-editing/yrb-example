# frozen_string_literal: true

require_relative "boot"

require "active_record/railtie"
require "active_storage/engine"
require "active_job/railtie"
require "action_mailer/railtie"
require "action_controller/railtie"
require "action_view/railtie"
require "action_cable/engine"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module YrbExample
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    # Use rspec
    config.generators.test_framework = :rspec

    # Use sidekiq backend
    config.active_job.queue_adapter = :sidekiq
  end
end
