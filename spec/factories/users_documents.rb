# frozen_string_literal: true

FactoryBot.define do
  factory :users_document do
    user { nil }
    document { nil }
    role { "MyString" }
  end
end
