# frozen_string_literal: true

FactoryBot.define do
  factory :diff do
    update { "MyString" }
    document { nil }
  end
end
